var popupContactAssistant = window.popupContactAssistant = {

    $content: null,
    container: '',

    init: function (content, container) {

        this.$content = content;
        this.container = container.toString();

        this.bind();
    },
    bind: function () {

        var self = this;

        $('body').append(this.$content);

        $('.level-2-contact-assistant').hide();

        $('.level-1-contact-assistant').on('click', function(e){
           $('.level-1-contact-assistant').hide();
            let $this = e.currentTarget;
            let parent = $this.parentNode;

            let children = parent.getElementsByClassName('level-2-contact-assistant');

            $.each(children, function(index, value){

                $(value).show();
            });
        });

        $('.back-level-1-contact-assistant').on('click', function(){
            $('.level-1-contact-assistant').show();
            $('.level-2-contact-assistant').hide();
        })

    }

}