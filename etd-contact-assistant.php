<?php
namespace Grav\Plugin;

use Grav\Common\Plugin;
use Grav\Common\Utils;

/**
 * Class ContactAssistantPlugin
 * @package Grav\Plugin
 */
class EtdContactAssistantPlugin extends Plugin
{
    static $plugin_config;

    /**
     * Init contact assistant template.
     * @type string
     */
    private $initTemplate = 'partials/etd-contact-assistant.html.twig';


    /**
     * @return array
     *
     * The getSubscribedEvents() gives the core a list of events
     *     that the plugin wants to listen to. The key of each
     *     array section is the event that the plugin listens to
     *     and the value (in the form of an array) contains the
     *     callable (or function) as well as the priority. The
     *     higher the number the higher the priority.
     */
    public static function getSubscribedEvents()
    {
        return [
            'onPluginsInitialized' => ['onPluginsInitialized', 0]
        ];
    }

    /**
     * Initialize the plugin
     */
    public function onPluginsInitialized()
    {
        // Don't proceed if we are in the admin plugin
        if ($this->isAdmin()) {
            return;
        }

        // Enable the main event we are interested in
        $this->enable([
            'onTwigTemplatePaths' => ['onTwigTemplatePaths', 0],
            'onTwigSiteVariables' => ['onTwigSiteVariables', 0]
        ]);

        self::$plugin_config = $this->config();

    }

    /**
     * Push plugin templates to twig paths array.
     */
    public function onTwigTemplatePaths()
    {
        // Push own templates to twig paths
        array_push(
            $this->grav['twig']->twig_paths,
            __DIR__ . '/templates'
        );
    }

    public function onTwigSiteVariables()
    {
        $contactAssistant = isset($_COOKIE['contact-assistant']) ? (string) $_COOKIE['contact-assistant'] : '';
        $config = self::$plugin_config;

        if(isset($config['enabled']) && $config['enabled'] == 1){
            if ($contactAssistant !== "ok") {

                $this->grav['assets']->addJs('plugin://etd-contact-assistant/assets/js/popup-contact-assistant.js', '', 'bottom');

                $twig = $this->grav['twig'];
                $this->grav['assets']->addInlineJs($twig->twig->render('partials/etd-contact-assistant.html.twig', ['configContactAssistant' => $config]), '', 'bottom');
            }
        }



    }



}

